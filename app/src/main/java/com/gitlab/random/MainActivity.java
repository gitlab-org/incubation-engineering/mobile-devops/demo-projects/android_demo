package com.gitlab.random;

import com.gitlab.random.BuildConfig;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {
    FloatingActionButton refreshBtn;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        refreshBtn = findViewById(R.id.refreshBtn);
        imageView = findViewById(R.id.randomImage);

        String versionCode = String.valueOf(BuildConfig.VERSION_CODE);
        String versionName = String.valueOf(BuildConfig.VERSION_NAME);

        Toast.makeText(MainActivity.this, versionName + " : " + versionCode, Toast.LENGTH_LONG).show();

        refreshBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) { loadImage(); }
        });
    }

    public void loadImage(){
        Picasso.get()
                .load("https://source.unsplash.com/random/1200x1200")
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .networkPolicy(NetworkPolicy.NO_CACHE).into(imageView);
    }
}